const Migrations = artifacts.require("ExpenseFactory");

module.exports = async function(_deployer, _, accounts) {
    await _deployer.deploy(Migrations);
    await web3.eth.sendTransaction({
        from: accounts[0],
        to: '0x9027398fEd66D8396b17BaA7219AA99D63D3Cb12',
        value: web3.utils.toWei('1', 'ether')
    });

};
