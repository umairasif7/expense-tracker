const Web3 = require('web3');
const MyContract = require('./build/contracts/ExpenseFactory');
const address = '0x18A95A8334B81cee5D25270A0f775D63D99507a6';
const privateKey = '0x96c96a3a0af755925d454b748867e6e8fece86232469c6cdadfd1f792acf8f68';
const HDWalletProvider = require('@truffle/hdwallet-provider');

const init = async () => {
    const provider = new HDWalletProvider(
        privateKey,
        'https://ropsten.infura.io/v3/8fbef695386842a8a3f89beae775bd60'
    );
    const web3 = new Web3(provider);
    // const id = await web3.eth.net.getId();
    // const deployedNetwork = MyContract.networks[id];
    let contract = new web3.eth.Contract(
        MyContract.abi,
    );
    contract = await contract
        .deploy({data: MyContract.bytecode})
        .send({from: address});
    // contract.deploy({
    //     data: MyContract.bytecode,
    // }).send({
    //     from: address,
    //     gas: 1500000,
    //     gasPrice: '80000000'
    // }, function (error, transactionHash) {
    //
    // }).on('error', function (error) {
    //     console.log('error', error);
    // }).on('transactionHash', function (transactionHash) {
    //     console.log('transactionHash', transactionHash);
    // }).on('receipt', function (receipt) {
    //     console.log('receipt', receipt.contractAddress);
    // }).on('confirmation', function (confirmationNumber, receipt) {
    //     console.log('confirmation', confirmationNumber);
    // });

    console.log(contract);

    const name = await contract.methods.name().call();
    const addresses = await web3.eth.getAccounts();
    const d = await contract.methods.createTransaction(address, '1', 'income', 14).send({
        from: address,
        gas:3000000
    });

    const result = await contract.methods.getTransactionsByOwner().call();
    for(i = 0; i < result.length; i++) {
        const record = await contract.methods.transactions(result[i]).call();
        console.log(record);
    }
};

init();