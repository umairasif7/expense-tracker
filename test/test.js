const Transaction = artifacts.require('./ExpenseFactory.sol');

require('chai')
    .use(require('chai-as-promised'))
    .should()

contract('IncomeExpense', ([deployer, author, tipper]) => {
    let transaction

    before(async () => {
        transaction = await Transaction.deployed()
    })

    describe('deployment', async () => {
        it('deploys successfully', async () => {
            const address = await transaction.address
            assert.notEqual(address, 0x0)
            assert.notEqual(address, '')
            assert.notEqual(address, null)
            assert.notEqual(address, undefined)
        })

        it('has a name', async () => {
            const name = await transaction.name()
            assert.equal(name, 'Expense Tracker')
        })
    })
    describe('transaction', async () => {
        let result;
        before(async () => {
            result = await transaction.createTransaction('Expense 1', 'income', 100)
        })
        it('get transaction' , async () => {
            console.log(result)
        })
    })
})